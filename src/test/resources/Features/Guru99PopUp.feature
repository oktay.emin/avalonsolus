@popup
Feature: Pop Up Handling

Scenario: Handling Pop Up Windows
Given User opens browser
When User navigate to URL "http://demo.guru99.com/test/delete_customer.php"
And User enters customer ID "53920" to delete customer
And User clicks Submit button
Then User clicks OK button on the pop up window