$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("file:src/test/resources/Features/amazonmousehover.feature");
formatter.feature({
  "name": "Amazon Mouse Hovering",
  "description": "",
  "keyword": "Feature"
});
formatter.scenario({
  "name": "Amazon with mouse hovering",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@mousehover"
    }
  ]
});
formatter.step({
  "name": "User launch Chrome Browser",
  "keyword": "Given "
});
formatter.match({
  "location": "com.avalon.stepdefinitions.AmazonStepDefinitions.user_launch_Chrome_Browser()"
});
formatter.result({
  "error_message": "java.lang.NoSuchMethodError: com.google.common.collect.ImmutableList.toImmutableList()Ljava/util/stream/Collector;\r\n\tat org.openqa.selenium.chrome.ChromeOptions.asMap(ChromeOptions.java:292)\r\n\tat org.openqa.selenium.remote.NewSessionPayload.create(NewSessionPayload.java:94)\r\n\tat org.openqa.selenium.remote.ProtocolHandshake.createSession(ProtocolHandshake.java:68)\r\n\tat org.openqa.selenium.remote.HttpCommandExecutor.execute(HttpCommandExecutor.java:136)\r\n\tat org.openqa.selenium.remote.service.DriverCommandExecutor.execute(DriverCommandExecutor.java:83)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.execute(RemoteWebDriver.java:552)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.startSession(RemoteWebDriver.java:213)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.\u003cinit\u003e(RemoteWebDriver.java:131)\r\n\tat org.openqa.selenium.chrome.ChromeDriver.\u003cinit\u003e(ChromeDriver.java:181)\r\n\tat org.openqa.selenium.chrome.ChromeDriver.\u003cinit\u003e(ChromeDriver.java:168)\r\n\tat org.openqa.selenium.chrome.ChromeDriver.\u003cinit\u003e(ChromeDriver.java:123)\r\n\tat com.avalon.stepdefinitions.AmazonStepDefinitions.user_launch_Chrome_Browser(AmazonStepDefinitions.java:30)\r\n\tat ✽.User launch Chrome Browser(file:///C:/Users/o.emin/eclipse-workspace/AVALONSOLUS/src/test/resources/Features/amazonmousehover.feature:5)\r\n",
  "status": "failed"
});
formatter.step({
  "name": "User Opens URL \"https://www.amazon.com\"",
  "keyword": "When "
});
formatter.match({
  "location": "com.avalon.stepdefinitions.AmazonStepDefinitions.user_Opens_URL(java.lang.String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User moves the mouse over Account and List",
  "keyword": "And "
});
formatter.match({
  "location": "com.avalon.stepdefinitions.AmazonStepDefinitions.user_moves_the_mouse_over_Account_and_List()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User should see account and list options",
  "keyword": "Then "
});
formatter.match({
  "location": "com.avalon.stepdefinitions.AmazonStepDefinitions.user_should_see_account_and_list_options()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "close browser",
  "keyword": "And "
});
formatter.match({
  "location": "com.avalon.stepdefinitions.StepDefinitions.close_browser()"
});
formatter.result({
  "status": "skipped"
});
});