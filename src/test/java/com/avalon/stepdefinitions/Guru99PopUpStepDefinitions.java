package com.avalon.stepdefinitions;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.github.bonigarcia.wdm.WebDriverManager;

public class Guru99PopUpStepDefinitions {
	WebDriver driver;

	@Given("User opens browser")
	public void user_opens_browser() {
		WebDriverManager.chromedriver().setup();
		driver = new ChromeDriver();
		driver.manage().window().maximize();

	}

	@When("User navigate to URL {string}")
	public void user_navigate_to_URL(String url) {
		driver.get(url);

	}

	@When("User enters customer ID {string} to delete customer")
	public void user_enters_customer_ID_to_delete_customer(String ID) {
		driver.findElement(By.name("cusid")).sendKeys(ID);
		
		String timeStamp = new SimpleDateFormat("yyyy_MM_dd__hh_mm_ss").format(new Date());
		TakesScreenshot ts = (TakesScreenshot)driver;
		File source = ts.getScreenshotAs(OutputType.FILE);
		try {
			FileUtils.copyFile(source, new File("C:\\Users\\oktay\\eclipse-workspace\\AVALONSOLUS" + "/SCREENSHOTS/Guru99 Pop Up Handling" +  timeStamp + ".png"));
		} catch (IOException e) {
			e.printStackTrace();
		}
		System.out.println("The Screenshot is taken");	
		
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {			
			e.printStackTrace();
		}

	}

	@When("User clicks Submit button")
	public void user_clicks_Submit_button() {

		driver.findElement(By.name("submit")).submit();

		Alert alert = driver.switchTo().alert();
		// Capturing alert message.
		String alertMessage = driver.switchTo().alert().getText();

		// Displaying alert message
		System.out.println(alertMessage);
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		// Accepting alert
		alert.accept();

	}

	@Then("User clicks OK button on the pop up window")
	public void user_clicks_OK_button_on_the_pop_up_window() {
		driver.close();
	}

}
