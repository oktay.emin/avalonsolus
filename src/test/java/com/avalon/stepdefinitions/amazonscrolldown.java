package com.avalon.stepdefinitions;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import io.cucumber.java.en.*;
import io.github.bonigarcia.wdm.WebDriverManager;

public class amazonscrolldown {
	
	
	WebDriver driver; 
	
	@Given("User should launch Chrome Browser")
	public void user_should_launch_Chrome_Browser() {
		WebDriverManager.chromedriver().setup();
		driver = new ChromeDriver();
		driver.manage().window().maximize();		
	}

	@When("User should Opens URL {string}")
	public void user_should_Opens_URL(String url) {
		driver.get(url);
	}

	@When("User scrolls down the page")
	public void user_scrolls_down_the_page() {
		
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("window.scrollBy(0, 550)");
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		//This code will scroll down all the way to the bottom of the page.
		js.executeScript("window.scrollBy(550,document.body.scrollHeight)");
		
	}

	@Then("User should see all other options")
	public void user_should_see_all_other_options() {
		String timeStamp = new SimpleDateFormat("yyyy_MM_dd__hh_mm_ss").format(new Date());
		TakesScreenshot ts = (TakesScreenshot)driver;
		File source = ts.getScreenshotAs(OutputType.FILE);
		try {
			FileUtils.copyFile(source, new File("C:\\Users\\oktay\\eclipse-workspace\\AVALONSOLUS" + "/SCREENSHOTS/Amazon Scroll Down: " +  timeStamp + ".png"));
		} catch (IOException e) {
			e.printStackTrace();
		}
		System.out.println("The Screenshot is taken");	
		
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {			
			e.printStackTrace();
		}
		
		
		
	}
	
	
	

	@Then("close chrome browser")
	public void close_chrome_browser() {
		driver.close();
		
	}




}
