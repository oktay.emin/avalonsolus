package com.avalon.utilities;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AvalonBrowserUtils {

	public WebDriver driver;
	
	public AvalonBrowserUtils(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(xpath = "//select[@name='orderby']")
	@CacheLookup
	WebElement sortButton;
	
	
	
	public void clickSortBtn() {
		sortButton.click();
	}
	
}
