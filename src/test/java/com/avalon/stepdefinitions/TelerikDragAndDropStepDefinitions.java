package com.avalon.stepdefinitions;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.io.FileUtils;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.github.bonigarcia.wdm.WebDriverManager;

public class TelerikDragAndDropStepDefinitions {
	WebDriver driver;
			
	@Given("User opens Chrome Browser")
	public void user_opens_Chrome_Browser() {
	WebDriverManager.chromedriver().setup();
	driver = new ChromeDriver();
	driver.manage().window().maximize();		
		
	}

	@When("User opens telerik URL {string}")
	public void user_opens_telerik_URL(String url) {
		driver.get(url);
	}

	@When("User should drag and drop the small circle into the big circle")
	public void user_should_drag_and_drop_the_small_circle_into_the_big_circle() {
		
		WebElement SourceElement = driver.findElement(By.id("draggable"));
		WebElement TargetElement = driver.findElement(By.id("droptarget"));
		Actions act = new Actions(driver);
		
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		act.dragAndDrop(SourceElement, TargetElement).build().perform();
		
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
	}

	
	@Then("User should see a message {string} inside the circle")
	public void user_should_see_a_message_inside_the_circle(String message) {
				
		if (driver.getPageSource().contains("You did great!")) {				
			System.out.println("Drag and Drop Successful");							
		} else {
			Assert.assertNotEquals(message, driver.getPageSource());				
				System.out.println("Drag and Drop Unseccessful");			
				Assert.assertTrue(true);
				driver.close();
		}
		
		
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e1) {
			e1.printStackTrace();
		}
		
		String timeStamp = new SimpleDateFormat("yyyy_MM_dd__hh_mm_ss").format(new Date());
		TakesScreenshot ts = (TakesScreenshot)driver;
		File source = ts.getScreenshotAs(OutputType.FILE);
		try {
			FileUtils.copyFile(source, new File("C:\\Users\\oktay\\eclipse-workspace\\AVALONSOLUS" + "/SCREENSHOTS/Telerik Drag and Drop" +  timeStamp + ".png"));
		} catch (IOException e) {
			e.printStackTrace();
		}
		System.out.println("The Screenshot is taken");	
		
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {			
			e.printStackTrace();
		}
		
	}

	@Then("User closes chrome browser.")
	public void user_closes_chrome_browser() {
		
		driver.close();
	}


	
}
