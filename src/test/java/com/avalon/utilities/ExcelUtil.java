package com.avalon.utilities;

import java.io.FileInputStream;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ExcelUtil {
	XSSFWorkbook wbook;
	XSSFSheet sheet;

	public ExcelUtil() {
	        try {
	            FileInputStream fis = new FileInputStream("C:\\Users\\oktay\\eclipse-workspace\\AVALONSOLUS\\src\\main\\resources\\ExcelFile\\DataProvider.xlsx");
	            wbook = new XSSFWorkbook(fis);
	            sheet = wbook.getSheetAt(0);
	        } catch (Exception e) {
	            System.out.println(e.getMessage());
	        }
	        }

	public String getData(int sheetNumber, int rowValue, int cellValue) {
		sheet = wbook.getSheetAt(sheetNumber);
		String data = sheet.getRow(rowValue).getCell(cellValue).getStringCellValue();
		return data;
	}

}

