package com.avalon.stepdefinitions;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

import io.cucumber.java.en.*;
import io.github.bonigarcia.wdm.WebDriverManager;

public class demoQADoubleClick {
	WebDriver driver;

	@Given("User opens chrome browser")
	public void user_opens_chrome_browser() {
		WebDriverManager.chromedriver().setup();
		driver = new ChromeDriver();
		driver.manage().window().maximize();	
	}
	
	@Then("User open URL {string}")
	public void user_open_URL(String url) {
		driver.get(url);		
	}

	@Then("User double clicks button")
	public void user_double_clicks_button() {
		Actions myAction = new Actions(driver);
		WebElement dblClick = driver.findElement(By.xpath("//button[@id='doubleClickBtn']"));		
		myAction.doubleClick(dblClick).perform();        
	
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		} 
		
		String timeStamp = new SimpleDateFormat("yyyy_MM_dd__hh_mm_ss").format(new Date());
		TakesScreenshot ts = (TakesScreenshot)driver;
		File source = ts.getScreenshotAs(OutputType.FILE);
		try {
			FileUtils.copyFile(source, new File("C:\\Users\\oktay\\eclipse-workspace\\AVALONSOLUS" + "/SCREENSHOTS/DemoQA Double Click" +  timeStamp + ".png"));
		} catch (IOException e) {
			e.printStackTrace();
		}			
		
		
		
	}

	@Then("User should see double click alert window")
	public void user_should_see_double_click_alert_window() {	
				
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {			
			e.printStackTrace();
		}
	
		//To press OK on the pop up window
		Alert alert = driver.switchTo().alert();
		alert.accept();
		
		System.out.println("The Screenshot is taken");
		
	
		
	}
	
	

	@Then("User closes browser")
	public void user_closes_browser() {
		
		driver.close();
		
	}



	
	
}
