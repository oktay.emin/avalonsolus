package com.avalon.utilities;

import org.junit.Test;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import io.github.bonigarcia.wdm.WebDriverManager;

public class Test1 {
WebDriver driver;
@Test
public void PopUpHandling() {
	
// Alert Message handling
	WebDriverManager.chromedriver().setup();
	driver = new ChromeDriver();
	
    driver.get("http://demo.guru99.com/test/delete_customer.php");			
                        		
 	      	
    driver.findElement(By.name("cusid")).sendKeys("53920");					
    driver.findElement(By.name("submit")).submit();			
    		
    // Switching to Alert        
    Alert alert = driver.switchTo().alert();		
    		
    // Capturing alert message.    
    String alertMessage= driver.switchTo().alert().getText();		
    		
    // Displaying alert message		
    System.out.println(alertMessage);	
    try {
		Thread.sleep(5000);
	} catch (InterruptedException e) {
		e.printStackTrace();
	}
    		
    // Accepting alert		
    alert.accept();
    
    try {
		Thread.sleep(3000);
	} catch (InterruptedException e) {
		e.printStackTrace();
	}
    
    driver.close();
}
	 	
	
}
