@doubleclick
Feature: DemoQA Double Click

Scenario: Double Click
Given User opens chrome browser
Then User open URL "https://demoqa.com/tooltip-and-double-click/"
And User double clicks button
Then User should see double click alert window
And User closes browser
