package com.avalon.stepdefinitions;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import io.cucumber.java.en.*;
import io.github.bonigarcia.wdm.WebDriverManager;

public class iFrameStepDefinitions {
	WebDriver driver;

	@Given("User open browser")
	public void user_open_browser() {
		WebDriverManager.chromedriver().setup();
		driver = new ChromeDriver();
		driver.manage().window().maximize();
	}

	@When("User navigates to {string}")
	public void user_navigates_to(String url) {
		driver.get(url);
	}

	@Then("User navigared between frames")
	public void user_navigared_between_frames() {
		
		// Before Switching iFrame
		String timeStamp = new SimpleDateFormat("yyyy_MM_dd__hh_mm_ss").format(new Date());
		TakesScreenshot ts = (TakesScreenshot) driver;
		File source = ts.getScreenshotAs(OutputType.FILE);
		try {
			FileUtils.copyFile(source, new File("C:\\Users\\oktay\\eclipse-workspace\\AVALONSOLUS"
					+ "/SCREENSHOTS/iFrame Handling Before" + timeStamp + ".png"));
		} catch (IOException e) {
			e.printStackTrace();
		}
		System.out.println("The Screenshot is taken");

		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		// switching the frame by ID
		driver.switchTo().frame("a077aa5e");

		System.out.println("********We are switch to the iframe*******");
		driver.findElement(By.xpath("html/body/a/img")).click();

		System.out.println("*********We are done***************");

		// After Switching iFrame
		String timeStampAfter = new SimpleDateFormat("yyyy_MM_dd__hh_mm_ss").format(new Date());
		TakesScreenshot tsAfter = (TakesScreenshot) driver;
		File sourceAfter = tsAfter.getScreenshotAs(OutputType.FILE);
		try {
			FileUtils.copyFile(source, new File("C:\\Users\\oktay\\eclipse-workspace\\AVALONSOLUS"
					+ "/SCREENSHOTS/iFrame Handling After" + timeStampAfter + ".png"));
		} catch (IOException e) {
			e.printStackTrace();
		}
		System.out.println("The Screenshot is taken");

		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

	}

	@Then("User closes chrome browser")
	public void user_closes_chrome_browser() {

		driver.quit();
	}

}
