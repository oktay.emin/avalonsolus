package com.avalon.runners;

import org.junit.runner.RunWith;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;

@RunWith(Cucumber.class)

@CucumberOptions(features = "src\\test\\resources\\Features", glue = "com.avalon.stepdefinitions", 
		dryRun = false, plugin = {"pretty", "json:target/cucumber-reports/Cucumber.json", 
		"junit:target/cucumber-reports/Cucumber.xml",
		"html:test-output" }, monochrome = true, tags = { "@draganddrop" })


public class TelerikDragAndDropRunner {

}
