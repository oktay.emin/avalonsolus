@smoke, @functional
Feature: Sorting options on the seach result page

@tag1
  Scenario Outline: Sort by <option>
    Given the user is on the "https://avalonsolus.com/courses/" page
    When user sorts by "<option>"
    Then following product should be displayed on top
    
    |name |<name> |
    |price|<price>|
    

    Examples: 
   |   option   |                     name                                              	|price |
   |Alphabetical|Introduction LearnPress – LMS pluginIntroduction LearnPress – LMS plugin	| free |

 

 