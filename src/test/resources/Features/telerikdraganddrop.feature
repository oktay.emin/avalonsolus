@draganddrop
Feature: Drag and Drop

Scenario: Telerik Drag and Drop
Given User opens Chrome Browser 
When User opens telerik URL "https://demos.telerik.com/kendo-ui/dragdrop/index"
And User should drag and drop the small circle into the big circle
Then User should see a message "You did great!" inside the circle
And User closes chrome browser.
