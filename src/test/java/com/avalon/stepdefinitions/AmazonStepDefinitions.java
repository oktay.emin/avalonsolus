package com.avalon.stepdefinitions;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

import com.avalon.utilities.AmazonBrowserUtils;
import com.avalon.utilities.PropertyConfig;

import io.cucumber.java.en.*;
import io.github.bonigarcia.wdm.WebDriverManager;

public class AmazonStepDefinitions {
	
	WebDriver driver; 
	
	
	@Given("User launch Chrome Browser")
	public void user_launch_Chrome_Browser() {
		WebDriverManager.chromedriver().setup();
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		
	}

	@When("User Opens URL {string}")
	public void user_Opens_URL(String url) {		
		driver.get(url);
	}

	@When("User moves the mouse over Account and List")
	public void user_moves_the_mouse_over_Account_and_List() {
		WebElement we = driver.findElement(By.xpath("//*[@id=\"nav-link-accountList\"]/span[2]"));
		Actions act = new Actions(driver);
		act.moveToElement(we).perform();
		
	}

	@Then("User should see account and list options")
	public void user_should_see_account_and_list_options() {		
		
		String timeStamp = new SimpleDateFormat("yyyy_MM_dd__hh_mm_ss").format(new Date());
		TakesScreenshot ts = (TakesScreenshot)driver;
		File source = ts.getScreenshotAs(OutputType.FILE);
		try {
			FileUtils.copyFile(source, new File("C:\\Users\\oktay\\eclipse-workspace\\AVALONSOLUS" + "/SCREENSHOTS/Amazon DropDown List" +  timeStamp + ".png"));
		} catch (IOException e) {
			e.printStackTrace();
		}
		System.out.println("The Screenshot is taken");	
		
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {			
			e.printStackTrace();
		}
		
		driver.close();
	}	
	
}
