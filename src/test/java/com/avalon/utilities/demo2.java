package com.avalon.utilities;

import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import io.github.bonigarcia.wdm.WebDriverManager;

public class demo2 {
	static WebDriver driver;

	public static void main(String[] args) {
		WebDriverManager.chromedriver().setup();
		driver = new ChromeDriver();
		driver.get("https://demoqa.com/automation-practice-switch-windows-2/");
		String parentWindowHandle = driver.getWindowHandle();
		System.out.println("Parent window's handle ->" + parentWindowHandle);
		WebElement clickElement = driver.findElement(By.id("button1"));

		for (int i = 0; i < 3; i++) {
			clickElement.click();
			try {
				Thread.sleep(3000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			
			Set<String> allWindowHandles = driver.getWindowHandles();
			String lastWindowHandles = "";
			for(String handle : allWindowHandles) {
				System.out.println("Window Handle ->" + handle);
				System.out.println("Navigating to Google");
				driver.switchTo().window(handle);
				lastWindowHandles = handle;
				
				
			}
			
		}
	}
}