package com.avalon.utilities;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;

import io.github.bonigarcia.wdm.WebDriverManager;

public class AmazonBrowserUtils {
	
	public WebDriver driverOne;	
	public AmazonBrowserUtils(WebDriver driverTwo) {
		driverOne = driverTwo;
		PageFactory.initElements(driverTwo, this);
		}	
	
	public void mouseHover() {
		WebDriverManager.chromedriver().setup();
		WebDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://www.amazon.com");
		WebElement we = driver.findElement(By.xpath("//*[@id=\"nav-link-accountList\"]/span[2]"));
		Actions act = new Actions(driver);
		act.moveToElement(we).perform();
	}
	
	public void ScreenShot(){
		String timeStamp = new SimpleDateFormat("yyyy_MM_dd__hh_mm_ss").format(new Date());
		TakesScreenshot ts = (TakesScreenshot)driverOne;
		File source = ts.getScreenshotAs(OutputType.FILE);
		try {
			FileUtils.copyFile(source, new File("C:\\Users\\oktay\\eclipse-workspace\\AVALONSOLUS" + "/SCREENSHOTS/Screenshot" +  timeStamp + ".png"));
		} catch (IOException e) {
			e.printStackTrace();
		}
		System.out.println("The Screenshot is taken");	
	}
		
}