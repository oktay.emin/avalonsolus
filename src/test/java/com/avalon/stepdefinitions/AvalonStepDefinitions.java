package com.avalon.stepdefinitions;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.By.ByName;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

import com.avalon.utilities.BrowserUtils;
import com.avalon.utilities.PropertyConfig;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.github.bonigarcia.wdm.WebDriverManager;

public class AvalonStepDefinitions {		

public BrowserUtils browserUtil;
WebDriver driver;

@Given("the user is on the {string} page")
public void the_user_is_on_the_page(String string) {
	WebDriverManager.chromedriver().setup();
	WebDriver driver = new ChromeDriver();
	driver.get(PropertyConfig.getProperty("url"));
	
	Thread.holdsLock(5000);
	
	Select dropDown = new Select(driver.findElement(By.name("orderby")));
	
	driver.findElement(By.xpath("//select[@name='orderby']")).click();
	dropDown.selectByVisibleText("Alphabetical");
}

@When("user sorts by {string}")
public void user_sorts_by(String option) {
	//driver.findElement(By.xpath("//select[@name='orderby']")).click();
}


@Then("following product should be displayed on top")
public void following_product_should_be_displayed_on_top(String option, String name) {
	try {
		Thread.sleep(5000);
	} catch (InterruptedException e) {
		e.printStackTrace();
	}
	driver.close();
}
}
